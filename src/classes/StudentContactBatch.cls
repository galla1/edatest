public without sharing class StudentContactBatch implements Database.Batchable<sObject> {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
        'SELECT Id, Contact__c, LSUID__c, Aggregate_Match_ID_1__c, Aggregate_Match_ID_2__c, ' +
        'LSUEMAIL__c, EMAIL1__c, FRSTNAME__c, LASTNAME__c,GENDER__c,BIRTHDTE__c,CURRICCD__c,Campus__c,PHONE__c ' +
        'FROM Student__c ' +
        'WHERE Contact__c = null';

        return Database.getQueryLocator(query);
    }


    public void execute(Database.BatchableContext bc, List<Student__c> scope) {
        StudentService.matchStudents(scope);
    }


    public void finish(Database.BatchableContext bc) {
        
         EnrollmentContactBatch b = new EnrollmentContactBatch ();
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
    }

}