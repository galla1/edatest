public without sharing class FinancialContactBatch implements Database.Batchable<sObject> {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
        'SELECT Id, Contact__c, studentId__c ' +
        'FROM Financial_Aid_Record__c ' +
        'WHERE Contact__c = null AND studentId__c != null';

        return Database.getQueryLocator(query);
    }


    public void execute(Database.BatchableContext bc, List<Financial_Aid_Record__c> scope) {
        FinancialService.matchFinancial(scope);
    }


    public void finish(Database.BatchableContext bc) {

        /*
        StudentContactBatch b = new StudentContactBatch ();

        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
        */
    }

}