@isTest
private class EnrollmentServiceTest {

/*

Enrollment - Search for a Contact where  Enrollment.studentid = contact.X89_Number_LSU_ID__c and populate the enrollment.contact__c lookup with match or do nothing
if multiples found use oldest record create date.

*/

    @isTest static void testContactMatchingLSUnum() {
        TestFactory tf = new TestFactory();
        Enrollment__c e1 = tf.enrollment1;

        Contact c1 = tf.contact1;

        //insert a second contact, just to make sure we don't match on that
        Contact c2 = tf.contact2;

        EnrollmentService.matchEnrollments(new List<Enrollment__c>{e1});

        //see if it matched
        e1 = requeryEnrollment(e1);
        system.assertEquals(c1.Id, e1.Contact__c);
    }




       public static Enrollment__c requeryEnrollment(Enrollment__c eIn) {
        return [
            SELECT Id, Contact__c, studentId__c
            FROM Enrollment__c WHERE Id=:eIn.Id
        ];
    }
    
    
    
    	static testmethod void testEnrollmentContactBatch(){
        list<enrollment__c>c=new list<enrollment__c>();
        for(integer i=0;i<200;i++){
            enrollment__c s=new enrollment__c();
 
            s.studentId__c='896304873';
            c.add(s);
        }
        insert c;
           
  		Test.startTest();
      	EnrollmentContactBatch x = new EnrollmentContactBatch();
    	database.executeBatch(x);
    	Test.stopTest();
    }



    /*


String objectToQuery = 'Student__c';
Id objectId = 'a2L2M000000qpcE';

String objectToQuery = 'Contact';
Id objectId = '003R000001MFLLb';


String objectToQuery = 'Contact';
Id objectId = '003R000001MFLLb';

String objectToQuery = 'Enrollment__c';
Id objectId = 'a2sR0000000AHpv';


Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
Schema.DescribeSObjectResult r =  gd.get(objectToQuery).getDescribe();
Map<String, Schema.SObjectField> fsMap = r.fields.getMap();

List<String> fieldsToQuery = new List<String>();

for (String f: fsMap.keySet()) {
    Schema.SObjectField sof = fsMap.get(f);
	Schema.DescribeFieldResult dfr = sof.getDescribe();
    if (dfr.isUpdateable()) {
        fieldsToQuery.add(f);
    }
}


String query = 'SELECT ' + String.join(fieldsToQuery, ',') + ' FROM ' + objectToQuery + ' WHERE Id=\'' + objectId + '\'';
sObject so = Database.query(query);

Map<String, Object> fieldsMap = so.getPopulatedFieldsAsMap();
Map<String, Object> fieldsMapNoNulls = new Map<String, Object>();

for (String f: fieldsMap.keySet()) {
    Object fieldVal = fieldsMap.get(f);
    if (fieldVal != null) {
        fieldsMapNoNulls.put(f, fieldVal);
    }
}

system.debug('fieldsMapNoNulls: ' + JSON.serialize(fieldsMapNoNulls));

insert new debug__c(data__c=objectToQuery + ': ' + JSON.serialize(fieldsMapNoNulls));



Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {'dan@danpeter.com'};
mail.setToAddresses(toAddresses);
mail.setReplyTo('dan@danpeter.com');
mail.setSubject(objectToQuery);
mail.setBccSender(false);
mail.setUseSignature(false);
mail.setPlainTextBody(JSON.serialize(fieldsMapNoNulls));
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });



     */







}