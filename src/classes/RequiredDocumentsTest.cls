@isTest
private class RequiredDocumentsTest {

/*

Required_Documents - Search for a Contact where  Required_Documents.studentid = contact.X89_Number_LSU_ID__c and populate the Required_Documents.contact__c lookup with match or do nothing
if multiples found use oldest record create date.

*/

    @isTest static void testContactMatchingLSUnum() {
        TestFactory tf = new TestFactory();
        Required_Document__c e1 = tf.Required_Document1;

        Contact c1 = tf.contact1;

        //insert a second contact, just to make sure we don't match on that
        Contact c2 = tf.contact2;

        RequiredDocumentsService.matchRequired_Documents(new List<Required_Document__c>{e1});

        //see if it matched
        e1 = requeryRequired_Document(e1);
        system.assertEquals(c1.Id, e1.Contact__c);
    }




       public static Required_Document__c requeryRequired_Document(Required_Document__c eIn) {
        return [
            SELECT Id, Contact__c, LSUID__c
            FROM Required_Document__c WHERE Id=:eIn.Id
        ];
    }
    
    
    
    	static testmethod void testRequired_DocumentContactBatch(){
        list<Required_Document__c>c=new list<Required_Document__c>();
        for(integer i=0;i<200;i++){
            Required_Document__c s=new Required_Document__c();
 
            s.LSUID__c='896304873';
            c.add(s);
        }
        insert c;
           
  		Test.startTest();
      	RequiredDocumentsContactBatch x = new RequiredDocumentsContactBatch();
    	database.executeBatch(x);
    	Test.stopTest();
    }




    
    

    /*


String objectToQuery = 'Student__c';
Id objectId = 'a2L2M000000qpcE';

String objectToQuery = 'Contact';
Id objectId = '003R000001MFLLb';


String objectToQuery = 'Contact';
Id objectId = '003R000001MFLLb';

String objectToQuery = 'Required_Document__c';
Id objectId = 'a2sR0000000AHpv';


Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
Schema.DescribeSObjectResult r =  gd.get(objectToQuery).getDescribe();
Map<String, Schema.SObjectField> fsMap = r.fields.getMap();

List<String> fieldsToQuery = new List<String>();

for (String f: fsMap.keySet()) {
    Schema.SObjectField sof = fsMap.get(f);
	Schema.DescribeFieldResult dfr = sof.getDescribe();
    if (dfr.isUpdateable()) {
        fieldsToQuery.add(f);
    }
}


String query = 'SELECT ' + String.join(fieldsToQuery, ',') + ' FROM ' + objectToQuery + ' WHERE Id=\'' + objectId + '\'';
sObject so = Database.query(query);

Map<String, Object> fieldsMap = so.getPopulatedFieldsAsMap();
Map<String, Object> fieldsMapNoNulls = new Map<String, Object>();

for (String f: fieldsMap.keySet()) {
    Object fieldVal = fieldsMap.get(f);
    if (fieldVal != null) {
        fieldsMapNoNulls.put(f, fieldVal);
    }
}

system.debug('fieldsMapNoNulls: ' + JSON.serialize(fieldsMapNoNulls));

insert new debug__c(data__c=objectToQuery + ': ' + JSON.serialize(fieldsMapNoNulls));



Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {'dan@danpeter.com'};
mail.setToAddresses(toAddresses);
mail.setReplyTo('dan@danpeter.com');
mail.setSubject(objectToQuery);
mail.setBccSender(false);
mail.setUseSignature(false);
mail.setPlainTextBody(JSON.serialize(fieldsMapNoNulls));
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });



     */







}