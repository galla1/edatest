public without sharing class ApplicationService {

    public static void matchApplications(List<Application__c> applications) {

        List<Application__c> applicationsToUpdate = new List<Application__c>();

        //set of LSUID__c's
        Set<String> lsuIds = new Set<String>();

        //build a set of match keys (email|last|first, populated by workflow
        Set<String> matchKeys = new Set<String>();

        for (Application__c s: applications) {
            if (String.isNotBlank(s.LSUID__c)) {
                lsuIds.add(s.LSUID__c);
            }
            if (String.isNotBlank(s.Aggregate_Match_ID_1__c)) {
                matchKeys.add(s.Aggregate_Match_ID_1__c);
            }
            if (String.isNotBlank(s.Aggregate_Match_ID_2__c)) {
                matchKeys.add(s.Aggregate_Match_ID_2__c);
            }
            
        }

        //Some contact maps to easily look up the values we want
        Map<String, List<Contact>> lsuIdMap = new Map<String, List<Contact>>();
        Map<String, List<Contact>> matchIdMap = new Map<String, List<Contact>>();
        
        //query the contacts table, build some maps
        for (Contact c: [SELECT Id, X89_Number_LSU_ID__c, Aggregate_Match_ID_1__c, CreatedDate
                         FROM Contact
                         WHERE X89_Number_LSU_ID__c IN:lsuIds
                         OR Aggregate_Match_ID_1__c IN:matchKeys]) {

            if (String.isNotBlank(c.X89_Number_LSU_ID__c)) {
                if (lsuIdMap.containsKey(c.X89_Number_LSU_ID__c)) {
                    List<Contact> tempList = lsuIdMap.get(c.X89_Number_LSU_ID__c);
                    tempList.add(c);
                    lsuIdMap.put(c.X89_Number_LSU_ID__c, tempList);
                } else {
                    lsuIdMap.put(c.X89_Number_LSU_ID__c, new List<Contact>{c});
                }
            }

            if (String.isNotBlank(c.Aggregate_Match_ID_1__c)) {
                if (matchIdMap.containsKey(c.Aggregate_Match_ID_1__c)) {
                    List<Contact> tempList = matchIdMap.get(c.Aggregate_Match_ID_1__c);
                    tempList.add(c);
                    matchIdMap.put(c.Aggregate_Match_ID_1__c, tempList);
                } else {
                    matchIdMap.put(c.Aggregate_Match_ID_1__c, new List<Contact>{c});
                }
            }
        }

        //sort the lists in the maps by CreatedDate ASC
        for (String lsuId: lsuIdMap.keySet()) {
            List<Contact> tempList = lsuIdMap.get(lsuId);
            tempList = sortContacts(tempList);
            lsuIdMap.put(lsuId, tempList);
        }
        for (String m: matchIdMap.keySet()) {
            List<Contact> tempList = matchIdMap.get(m);
            tempList = sortContacts(tempList);
            matchIdMap.put(m, tempList);
        }

        List<Application__c> applicationsNoContactsFound = new List<Application__c>();

        //do a first pass to try to match Applications on Contacts
        for (Application__c s: applications) {
            //look for existing contact matching Application__c.LSUID__c on Contact.X89_Number_LSU_ID__c
            //if multiples found, use oldest CreatedDate

            Boolean foundMatch = false;
            if (String.isNotBlank(s.LSUID__c)) {
                if (lsuIdMap.containsKey(s.LSUID__c)) {
                    List<Contact> tempList = lsuIdMap.get(s.LSUID__c);
                    s.Contact__c = tempList[0].Id; //get the first in the list, the oldest
                    applicationsToUpdate.add(s);
                    foundMatch = true;
                     system.debug('Temp List Contact:'+tempList[0].Id);
                }
            }

            if (foundMatch) {
                continue;
            }

            //if we didn't find a match yet
            //look for match in our contact match key map (email|last|first) with the Aggregate_Match_ID_1__c
            if (String.isNotBlank(s.Aggregate_Match_ID_1__c)) {
                if (matchIdMap.containsKey(s.Aggregate_Match_ID_1__c)) {
                    List<Contact> tempList = matchIdMap.get(s.Aggregate_Match_ID_1__c);
                    s.Contact__c = tempList[tempList.size()-1].Id; //get the last in the list, the newest
                    applicationsToUpdate.add(s);
                    foundMatch = true;
                    system.debug('Temp List Contact:'+tempList[tempList.size()-1].Id);
                }
            }

            if (foundMatch) {
                continue;
            }

            //if we didn't find a match yet
            //look for match in our contact match key map (email|last|first) with the Aggregate_Match_ID_2__c
            if (String.isNotBlank(s.Aggregate_Match_ID_2__c)) {
                if (matchIdMap.containsKey(s.Aggregate_Match_ID_2__c)) {
                    List<Contact> tempList = matchIdMap.get(s.Aggregate_Match_ID_2__c);
                    s.Contact__c = tempList[tempList.size()-1].Id; //get the last in the list, the newest
                    applicationsToUpdate.add(s);
                    foundMatch = true;
                }
            }

            if (!foundMatch) {
                //add applications with noContacts owner to the Director of Retention 
                /*for(Application__c a: applicationsNoContactsFound){
                    a.OwnerId = defaultUser.Id;
                }
                update applicationsNoContactsFound; */              
                applicationsNoContactsFound.add(s);
            }
        }


        //==================  BEGIN checking Contacts

        //do pass for any Applications we didn't find in Contacts.  We are going to check Contacts next.

        //set of LSUID__c's
        Set<String> lsuIdsNoContacts = new Set<String>();

        //build a set of match keys (email|last|first, populated by workflow
        Set<String> matchKeysNoContacts = new Set<String>();

        for (Application__c s: applicationsNoContactsFound) {
            if (String.isNotBlank(s.LSUID__c)) {
                lsuIdsNoContacts.add(s.LSUID__c);
            }
            if (String.isNotBlank(s.Aggregate_Match_ID_1__c)) {
                matchKeysNoContacts.add(s.Aggregate_Match_ID_1__c);
            }
            if (String.isNotBlank(s.Aggregate_Match_ID_2__c)) {
                matchKeysNoContacts.add(s.Aggregate_Match_ID_2__c);
            }
        }

        //Build some Contact maps to easily look up the values we want
        Map<String, List<Contact>> lsuIdContactMap = new Map<String, List<Contact>>();
        Map<String, List<Contact>> matchIdContactMap = new Map<String, List<Contact>>();
		
        
        //query the Contacts table, build some maps
        for (Contact l: [SELECT Id, X89_Number_LSU_ID__c, Aggregate_Match_ID_1__c,AccountId,OwnerId
                      FROM Contact
                      WHERE (X89_Number_LSU_ID__c IN:lsuIdsNoContacts
                      OR Aggregate_Match_ID_1__c IN:matchKeysNoContacts)
                     ]) {
/* */
            if (String.isNotBlank(l.X89_Number_LSU_ID__c)) {
                //if (lsuIdContactMap.containsKey(l.X89_Number_LSU_ID__c)) {
                if (lsuIdContactMap.containsKey(l.X89_Number_LSU_ID__c)) {
                    List<Contact> tempList = lsuIdContactMap.get(l.X89_Number_LSU_ID__c);
                    tempList.add(l);
                    lsuIdContactMap.put(l.X89_Number_LSU_ID__c, tempList);
                } else {
                    lsuIdContactMap.put(l.X89_Number_LSU_ID__c, new List<Contact>{l});
                }
            }

            if (String.isNotBlank(l.Aggregate_Match_ID_1__c)) {
                //if (matchIdContactMap.containsKey(l.Aggregate_Match_ID_1__c)) {
                if (matchIdContactMap.containsKey(l.Aggregate_Match_ID_1__c)) {
                    List<Contact> tempList = matchIdContactMap.get(l.Aggregate_Match_ID_1__c);
                    tempList.add(l);
                    matchIdContactMap.put(l.Aggregate_Match_ID_1__c, tempList);
                } else {
                    matchIdContactMap.put(l.Aggregate_Match_ID_1__c, new List<Contact>{l});
                }
            }
        }

        //system.debug('matchIdContactMap: ' + matchIdContactMap);

        //Matching remaining applications not found in Contacts, checking Contacts

        List<Contact> leadsToConvert = new List<Contact>();
        List<Account> accountId = new List<Account>();
        List<Contact> contactsToCreate = new List<Contact>();
		List<Interaction__c> interactionsToCreate = new List<Interaction__c>();
        List<Application__c> applicationsContactConvert = new List<Application__c>();
        List<Application__c> applicationsNewContact = new List<Application__c>();


        for (Application__c s: applicationsNoContactsFound) {
            //look for existing Contact matching Application__c.LSUID__c on Contact.X89_Number_LSU_ID__c
            //if one found, add it to a list to convert
            //if more than one found, we are going to create a new contact

            Boolean foundMatch = false;
            if (String.isNotBlank(s.LSUID__c)) {
                if (lsuIdContactMap.containsKey(s.LSUID__c)) {
                    List<Contact> tempList = lsuIdContactMap.get(s.LSUID__c);
                    if (tempList.size() == 1) {
                        leadsToConvert.add(tempList[0]);
                        applicationsContactConvert.add(s);
                    } else {
                        //contactsToCreate.add(createContactFromapplication(s));
                        interactionsToCreate.add(createInteractionFromApplication(s));
                        applicationsNewContact.add(s);
                    }
                    foundMatch = true;
                }
            }

            if (foundMatch) {
                continue;
            }

            //if we didn't find a match yet
            //look for match in our Contact match key map (email|last|first) with the Aggregate_Match_ID_1__c
            if (String.isNotBlank(s.Aggregate_Match_ID_1__c)) {
                if (matchIdContactMap.containsKey(s.Aggregate_Match_ID_1__c)) {
                    List<Contact> tempList = matchIdContactMap.get(s.Aggregate_Match_ID_1__c);
                    if (tempList.size() == 1) {
                        //QUESTION: do we update LSUID__c on Contact with the value from application?
                        leadsToConvert.add(tempList[0]);
                        applicationsContactConvert.add(s);
                    } else {
                        //contactsToCreate.add(createContactFromapplication(s));                        
                        interactionsToCreate.add(createInteractionFromApplication(s));
                        applicationsNewContact.add(s);
                    }
                    foundMatch = true;
                }
            }

            if (foundMatch) {
                continue;
            }

            //if we didn't find a match yet
            //look for match in our Contact match key map (email|last|first) with the Aggregate_Match_ID_2__c
            if (String.isNotBlank(s.Aggregate_Match_ID_2__c)) {
                if (matchIdContactMap.containsKey(s.Aggregate_Match_ID_2__c)) {
                    List<Contact> tempList = matchIdContactMap.get(s.Aggregate_Match_ID_2__c);
                    if (tempList.size() == 1) {
                        //QUESTION: do we update LSUID__c on Contact with the value from application?
                        leadsToConvert.add(tempList[0]);
                        applicationsContactConvert.add(s);
                    } else {
                        //contactsToCreate.add(createContactFromapplication(s));                        
                        interactionsToCreate.add(createInteractionFromApplication(s));
                        applicationsNewContact.add(s);
                    }
                    foundMatch = true;
                }
            }
            if (!foundMatch) {
               // contactsToCreate.add(createContactFromapplication(s));
                interactionsToCreate.add(createInteractionFromApplication(s));
                //add application owner to the Director of Retention if there is no existing lead
              /*  for(Application__c a: applicationsNewContact){
                    a.OwnerId = defaultUser.Id;
                }
                update applicationsNewContact;*/
                applicationsNewContact.add(s);
            }

        }

       // system.debug('leadsToConvert: ' + leadsToConvert);
       // system.debug('applicationsContactConvert: ' + applicationsContactConvert);

        //===================  BEGIN Contact Conversion ===========

        //now process all of our collections: Updates, Inserts, Converts.
        //TODO: partial successes?  error handling?
        //
        /*

        
        List<Database.ContactConvert> lcs = new list<Database.ContactConvert>();
        
        for (Contact l: leadsToConvert) {
            if(l.OwnerId == leadOwnerId.Id && l.Company =='Individuals' ){ 
                l.OwnerId = defaultUser.Id;
                //l.Contact_Applicant_Pipeline_Status__c = 'Application';
                system.debug('UserOrGroupId: ' + l.OwnerId);
                Database.ContactConvert lc = new Database.ContactConvert();
                lc.setContactId(l.Id);
                lc.setAccountId(accId.get(0).Id);
                lc.setOwnerId(l.OwnerId);
                lc.setConvertedStatus(LEAD_CONVERT_STATUS);
                lc.setDoNotCreateOpportunity(true);                
                lcs.add(lc);
            }            
            else if(l.OwnerId == leadOwnerId.Id && l.Company !='Individuals' ){ 
                l.OwnerId = defaultUser.Id;
                //l.Contact_Applicant_Pipeline_Status__c = 'Application';
                system.debug('UserOrGroupId: ' + l.OwnerId);
                Database.ContactConvert lc = new Database.ContactConvert();
                lc.setContactId(l.Id);
                lc.setOwnerId(l.OwnerId);
                lc.setConvertedStatus(LEAD_CONVERT_STATUS);
                lc.setDoNotCreateOpportunity(true);                
                lcs.add(lc);
            }
            else{              
                //l.Contact_Applicant_Pipeline_Status__c = 'Application';
				Database.ContactConvert lc = new Database.ContactConvert();
                lc.setContactId(l.Id);                
                lc.setAccountId(accId.get(0).Id);
                lc.setConvertedStatus(LEAD_CONVERT_STATUS);
                lc.setDoNotCreateOpportunity(true);
                lcs.add(lc); 
            }  
            Update l;
        }

        //TODO: bulk test this, may need to break into batches of 100
        List<Database.ContactConvertResult> convertResults = Database.convertContact(lcs);

        //loop back through our applications that resulted in lead conversions and update the lookup to the newly created contact
        Integer intCount = 0;
        for (Database.ContactConvertResult lcr: convertResults) {
            if (lcr.isSuccess()) {
                applicationsContactConvert[intCount].Contact__c = lcr.getContactId();
                applicationsToUpdate.add(applicationsContactConvert[intCount]);
            }
            intCount++;
        }*/
        //===================  END Contact Conversion ===========


		
		//===================  BEGIN Interaction Insert  ===========
		system.debug('interactionsToCreate: ' + contactsToCreate);
        //Database.SaveResult[] myContactsLst = Database.insert(contactsToCreate,false);
        
     /*   for(Contact c: contactsToCreate)
                {
                    if(c.Owner.Profile.Name == 'System Administrator'){
                    c.OwnerId =defaultUser.Id;
                    }
                }  */
        // contactsToCreate.OwnerId =defaultUser.Id;
       //insert interactionsToCreate;
       
       Database.SaveResult[] myInteractionsLst = Database.insert(interactionsToCreate,false);
       Set<Id> myInteractionSet = new set<Id>();
      
		
		
         Integer intCount1 = 0;
        for (Application__c s: applicationsNewContact) {
            for(Database.SaveResult sr : myInteractionsLst){
                if(sr.isSuccess())
            {
                //Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted Interaction. Interaction ID:'+sr.getId()); 
                //get the Id of the newly created contact
            	//s.Contact__c = interactionsToCreate[intCount1].contact__c;            	
            	myInteractionSet.add(sr.getId());
                applicationsToUpdate.add(s);                
                intCount1++;
            }
            else{
                //Operation failed, so get all errors
                for(Database.Error err : sr.getErrors())
                {
                    System.debug('The following error has occured.');
                    System.debug(err.getStatusCode()+': '+err.getMessage());
                    System.debug('Contact fields that affected this error:'+ err.getFields());
                }
            }
            //get the Id of the newly created contact
           // s.Contact__c = interactionsToCreate[intCount1].Contact__c;

           /* applicationsToUpdate.add(s);
            intCount1++;*/
       		 }
            
        }
        
        List<Interaction__c> myInteractionList = [SELECT Contact__c,Affiliation_ID__c FROM Interaction__c WHERE Id IN:myInteractionSet];
        //s.Contact__c = myInteractionList[intCount1].Contact__c;
		//===================  END Interaction Insert  ===========
		
        
        
        
        //===================  BEGIN Contact Insert  ===========
        system.debug('contactsToCreate: ' + contactsToCreate);
        //Database.SaveResult[] myContactsLst = Database.insert(contactsToCreate,false);
        
       /* for(Contact c: contactsToCreate)
                {
                    if(c.Owner.Profile.Name == 'System Administrator'){
                    c.OwnerId =defaultUser.Id;
                    }
                }  */
        // contactsToCreate.OwnerId =defaultUser.Id;
       // insert contactsToCreate;
       /* for(Database.SaveResult sr : myContactsLst){
            if(sr.isSuccess())
            {
                //Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted Contact. Contact ID:'+sr.getId());                
            }
            else{
                //Operation failed, so get all errors
                for(Database.Error err : sr.getErrors())
                {
                    System.debug('The following error has occured.');
                    System.debug(err.getStatusCode()+': '+err.getMessage());
                    System.debug('Contact fields that affected this error:'+ err.getFields());
                }
            }
        }*//*
         Integer intCount = 0;
        for (Application__c s: applicationsNewContact) {
            //get the Id of the newly created contact
            s.Contact__c = contactsToCreate[intCount].Id;

            applicationsToUpdate.add(s);
            intCount++;
        }*/
        //===================  END Contact Insert  ===========
		integer intCount2=0;
       //loop through the applicationsToUpdate list and set the Id / text field
        for (Application__c s: applicationsNewContact) {
            s.Contact__c = myInteractionList[intCount2].Contact__c; 
            s.LSU_Affiliation__c= myInteractionList[intCount2].Affiliation_ID__c;
            intCount2++;
        }

 


        Boolean doUpdate = false;
        if (Test.isRunningTest()) {
            doUpdate = true;
        } else {
            if (System.isBatch()) {
                doUpdate = true;
            } else {
                if (Trigger.isAfter) {
                    doUpdate = true;
                }
            }
        }

        if (doUpdate) {
            update applicationsToUpdate;
        }

    }
    public static Interaction__c createInteractionFromApplication(Application__c s) {
	
        String emailToUse = s.Email_1__c;
        String Gender;
        Boolean emailValid = true;
	    List<Plan__c> planId = [SELECT Id, Department__c
                              FROM Plan__c 
                             WHERE Name =: s.Program_Code__c LIMIT 1];
        List<Account> accId = [SELECT Id 
                              FROM Account 
                             WHERE Name =: s.Campus__c LIMIT 1];
        if (String.isBlank(emailToUse)) {
            emailValid = false;
        } else {
            if (emailToUse == '*') {
                emailValid = false;
            }
        }

        if (!emailValid) {
            emailToUse = s.Email_2__c;            
        }
        if (String.isBlank(emailToUse)) {
            emailValid = false;
        } else {
            if (emailToUse == '*') {
                emailValid = false;
            }
        }
        if (!emailValid) {
            emailToUse = s.Institution_Email__c;            
        }
        
        
        
		List<Application__c> apps = [SELECT Id, Gender__c 
                                       FROM Application__c 
                                      WHERE Id=:s.Id];
        for(Application__c app: apps){
            if(app.Id == s.Id){
                if(app.Gender__c!=NULL && app.Gender__c=='M')
                    Gender = 'Male';
                else if(app.Gender__c!=NULL && app.Gender__c=='F')
                    Gender = 'Female';
                else
                    Gender = NULL;
            }
        }
        return new Interaction__c(
            First_Name__c 			= s.First_Name__c,
            Last_Name__c 			= s.Last_Name__c,
            Email__c 				= emailToUse,
            Mobile_Phone__c 		= s.Cell_Phone__c,
            //Birthdate__c 			= date.parse(s.DOB__c),
            Gender__c				= Gender,
            Interaction_Source__c	= 'Application',
            University_Email__c 	= s.Institution_Email__c,
            LSUID__c 				= s.LSUID__c,
            Opportunity_Stage__c 	= 'Application',
            Academic_Interest__c 	= planId.get(0).Id,
            Affiliated_Account__c 	= planId.get(0).Department__c,
            Affiliation_Role__c 	= 'Student'
            //OwnerId 				= defaultUser.Id
        );
    }
/*
    public static Contact createContactFromApplication(Application__c s) {
	
        String emailToUse = s.Email_1__c;
        Boolean emailValid = true;*/
	   /* User defaultUser = [SELECT Id ,UserName, UserRole.Name 
                              FROM user 
                             WHERE UserRole.DeveloperName = 'Director_of_Online_Student_Recruitment'];*/
       /* if (String.isBlank(emailToUse)) {
            emailValid = false;
        } else {
            if (emailToUse == '*') {
                emailValid = false;
            }
        }

        if (!emailValid) {
            emailToUse = s.Email_2__c;
        }

        return new Contact(
            FirstName = s.First_Name__c,
            LastName = s.Last_Name__c,
            Email = emailToUse,
            X89_Number_LSU_ID__c = s.LSUID__c,
            OwnerId = defaultUser.Id
        );
    }*/


    public static List<Contact> sortContacts(List<Contact> contactsIn) {
        List<ContactWrap> wrapList = new List<ContactWrap>();
        List<Contact> contactsOut = new List<Contact>();

        for (Contact c: contactsIn) {
            wrapList.add(new ContactWrap(c));
        }
        wrapList.sort();

        for (ContactWrap w: wrapList) {
            contactsOut.add(w.c);
        }
        return contactsOut;
    }

    public class ContactWrap implements Comparable {
        public Contact c {get; set;}
        public ContactWrap(Contact con) {
            c = con;
        }
        //sorts by ASC
        public Integer compareTo(Object compareTo) {
            ContactWrap compareToContact = (ContactWrap)compareTo;
            if (c.CreatedDate > compareToContact.c.CreatedDate) {
                return 1;
            }
            if (c.CreatedDate < compareToContact.c.CreatedDate) {
                return -1;
            }
            return 0;
        }
    }
}