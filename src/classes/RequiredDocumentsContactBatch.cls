public without sharing class RequiredDocumentsContactBatch implements Database.Batchable<sObject> {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
        'SELECT Id, Contact__c,LSUID__c ' +
        'FROM Required_Document__c ' +
        'WHERE Contact__c = null AND LSUID__c != null';

        return Database.getQueryLocator(query);
    }


    public void execute(Database.BatchableContext bc, List<Required_Document__c> scope) {
        RequiredDocumentsService.matchRequired_Documents(scope);
    }


    public void finish(Database.BatchableContext bc) {

        /*
        StudentContactBatch b = new StudentContactBatch ();

        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
        */
    }

}