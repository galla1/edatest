public without sharing class ApplicationContactBatch implements Database.Batchable<sObject> {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
        'SELECT Id, Contact__c,Campus__c,LSUID__c, Aggregate_Match_ID_1__c,Aggregate_Match_ID_2__c, ' +
        'EMAIL_2__c, EMAIL_1__c, First_Name__c, Last_Name__c,Cell_Phone__c,DOB__c,Gender__c,Program_Name__c,Institution_Email__c,Program_Code__c ' +
        'FROM Application__c ' +
        'WHERE Contact__c = null';

        return Database.getQueryLocator(query);
    }


    public void execute(Database.BatchableContext bc, List<Application__c> scope) {
        ApplicationService.matchApplications(scope);
    }

    public void finish(Database.BatchableContext bc) {
          //RequiredDocumentsContactBatch b = new RequiredDocumentsContactBatch ();
        //Parameters of ExecuteBatch(context,BatchSize)
        //database.executebatch(b,200);
        
    }

}