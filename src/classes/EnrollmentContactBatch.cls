public without sharing class EnrollmentContactBatch implements Database.Batchable<sObject> {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
        'SELECT Id, Contact__c, studentId__c ' +
        'FROM Enrollment__c ' +
        'WHERE Contact__c = null AND studentId__c != null';

        return Database.getQueryLocator(query);
    }


    public void execute(Database.BatchableContext bc, List<Enrollment__c> scope) {
        EnrollmentService.matchEnrollments(scope);
    }


    public void finish(Database.BatchableContext bc) {

        
        FinancialContactBatch b = new FinancialContactBatch ();

        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
        
    }

}