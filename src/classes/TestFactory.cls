@isTest
public class TestFactory {


    public TestFactory() {
    }



    public Student__c student1 {
        get {
            if (student1 == null) {
                student1 = new Student__c(
                    Campus__c  = 'LSUAM',
                    Ext_Key__c  = '111111111_MBADM_LSUAM',
                    OVRLGPA__c  = '2.8',
                    TERMPAY2__c  = '20193D',
                    TPAYFLAG2__c  = 'N',
                    TERMPAY1__c  = '20192L',
                    TPAYFLAG1__c  = 'Y',
                    PAYSTATUS__c  = 'Y',
                    MODDEGDS__c  = 'STUDENT MODULE COMPLETED  = \'20202L\'',
                    MODDEGCD__c  = 'MC',
                    GRADDATE__c  = date.parse('05/01/2020'),
                    HOURS__c  = '6.00',
                    ACTLSTRT__c  = '08/27/2018',
                    ORIGTERM__c  = '20191L',
                    ADMTERM__c  = '20191L',
                    PGMADMDT__c  = '07/26/2018',
                    INSTDATE__c  = date.parse('08/22/2018'),
                    CURRICCD__c  = 'MBADM',
                    CURRICDS__c  = 'BUSINESS ADMIN MBA',
                    INTERNL__c  = 'N',
                    ETHNDESC__c  = 'White',
                    ETHNIC__c  = 'W',
                    BIRTHDTE__c  = date.parse('01/01/1901'),
                    GENDER__c  = 'Male',
                    RACEDESC__c  = 'White',
                    RACE__c  = 'W',
                    //LSUEMAIL__c  = 'test1@lsu.edu.invalid',
                    EMAIL1__c  = 'test1@lsu.edu.invalid',
                    PHONE__c  = '111-222-1234',
                    COUNTRY__c  = 'US',
                    ZIP__c  = '78154',
                    STATE__c  = 'TX',
                    CITY__c  = 'Schertz',
                    ADDR1__c  = '123 Texas Ln',
                    MIDDNAME__c  = 'JIM',
                    FRSTNAME__c  = 'JOE',
                    LASTNAME__c  = 'BLOW',
                    LSUID__c  = '111111111'
                );
                insert student1;
            }
            return student1;
        }
        set;
    }


    
    public Application__c application1 {
        get {
            if (application1 == null) {
                application1 = new application__c(
                    Campus__c  = 'LSUAM',
                    Ext_Key__c  = '111111111_MBADM_LSUAM',
                    //LSUEMAIL__c  = 'test1@lsu.edu.invalid',
                    Email_1__c  = 'test1@lsu.edu.invalid',
                    Cell_Phone__c  = '111-222-1234',
                    Permanent_Address_County__c  = 'US',
                    Permanent_Address_ZIP_Code__c  = '78154',
                    STATE__c  = 'TX',
                    CITY__c  = 'Schertz',
                    Address_Line_1__c  = '123 Texas Ln',
                    Middle_Name__c  = 'JIM',
                    First_Name__c  = 'JOE',
                    Last_Name__c  = 'BLOW',
                    LSUID__c  = '111111111'
                    //Aggregate_Match_ID_1__c = 'test1@lsu.edu.invalid|SMITH|JOE',
                    //Aggregate_Match_ID_2__c = 'test2@lsu.edu.invalid|SMITH|JOE'
                );
                insert application1;
            }
            return application1;
        }
        set;
    }




    public Enrollment__c enrollment1 {
        get {
            if (enrollment1 == null) {
                enrollment1 = new Enrollment__c(
                    Campus__c = 'LSUAM',
                    Ext_Key__c = '890013572_001-SW-7807-20191P_LSUAM',
                    Term__c = '20192L',
                    studentId__c = '111111111',
                    SectionID__c = '001-SW-7807-20191P',
                    Start_Date__c = date.parse('3/11/2019'),
                    End_Date__c = date.parse('4/26/2019'),
                    Role__c = 'STUDENT',
                    Status_Code__c = 'A',
                    Status_Desc__c = 'ACTIVE',
                    Status_Reason_Code__c = 'E',
                    Effective_Date__c = date.parse('2/20/2019'),
                    Program__c = 'MSW',
                    Final_Grade__c = '*',
                    Completion_Status_Code__c = '*',
                    Competition_Status_Desc__c = '*',
                    Base_Course_Tuition__c = '*',
                    Billable_Tuition__c = '*',
                    Billable_Fees__c = '*',
                    Waivers__c = '*',
                    statusReasonCode__c = 'ENROLLED',
                    creditHoursForCourse__c = '0'
                );
                insert enrollment1;
            }
            return enrollment1;
        }
        set;
    }
    
       public Financial_Aid_Record__c fiancial1 {
        get {
            if (fiancial1 == null) {
                fiancial1 = new Financial_Aid_Record__c(
                    Mainframe_Dataload_ID__c = '25489',
                    Ext_Key__c = '890013572_001-SW-7807-20191P_LSUAM',
                    Term__c = '20192L',
                    studentId__c = '111111111',
                    //Contact__c = '',
                    statusCode__c = 'A'
                    //statusDesc__c = 'ACTIVE'
                    );
                insert fiancial1;
            }
            return fiancial1;
        }
        set;
    }

    public Required_Document__c Required_Document1 {
        get {
            if (Required_Document1 == null) {
                Required_Document1 = new Required_Document__c(
                    ApplicationID__c = '3742603',
                    Campus__c = 'LSUAM',
                    Dataload_Unique_ID__c = '890013572_001-SW-7807-20191P_LSUAM',
                    Document_Name__c = 'TRANSCRIPT FROM: Texas State University - San Marcos',
                    Ext_Key__c = '3742603_896720486_TRANSCRIPT FROM: Texas State University - San Marcos_LSUAM',
                    LSUID__c = '111111111',
                    Received_Date__c = date.parse('10/01/2014'),
                    Review_Status_Code__c = '*',
                    Review_Status_Desc__c = '*'
                
                );
                insert Required_Document1;
            }
            return Required_Document1;
        }
        set;
    }

    public Contact contact1 {
        get {
            if (contact1 == null) {
                contact1 = new Contact(
                    FirstName = 'JOE',
                    LastName = 'BLOW',
                    Email='test1@lsu.edu.invalid',
                    X89_Number_LSU_ID__c='111111111'
                );
                insert contact1;
            }
            return contact1;
        }
        set;
    }


    public Contact contact2 {
        get {
            if (contact2 == null) {
                contact2 = new Contact(
                        FirstName = 'SALLY',
                        LastName = 'SMOTHERS',
                        Email='test555@lsu.edu.invalid',
                        X89_Number_LSU_ID__c='222222222'
                );
                insert contact2;
            }
            return contact2;
        }
        set;
    }

    


    public Lead lead1 {
        get {
            if (lead1 == null) {
                lead1 = new Lead(
                    FirstName = 'HOWY',
                    LastName = 'HOWARD',
                    Email='test3333@lsu.edu.invalid',
                    X89_Number_LSU_ID__c='333333333',
                    Company='N/A'
                );
                insert lead1;
            }
            return lead1;
        }
        set;
    }

    public Lead lead2 {
        get {
            if (lead2 == null) {
                lead2 = new Lead(
                    FirstName = 'HOWY2',
                    LastName = 'HOWARD2',
                    Email='test33332@lsu.edu.invalid',
                    X89_Number_LSU_ID__c='333333333', //same ID as above, different name
                    Company='N/A'
                );
                insert lead2;
            }
            return lead2;
        }
        set;
    }


}