@isTest
private class FinancialServiceTest {

/*

FinancialAid - Search for a Contact where  FinancialAid.studentid = contact.X89_Number_LSU_ID__c and populate the FinancialAid.contact__c lookup with match or do nothing
if multiples found use oldest record create date.

*/

    @isTest static void testContactMatchingLSUnum() {
        TestFactory tff = new TestFactory();
        Financial_Aid_Record__c f1 = tff.fiancial1;

        Contact c1 = tff.contact1;
        //c1. X89_Number_LSU_ID__c='898523569';
       // Update c1;
        //insert a second contact, just to make sure we don't match on that
        Contact c2 = tff.contact2;
        //C2. X89_Number_LSU_ID__c='898523569';
        //Update c2;
        FinancialService.matchFinancial(new List<Financial_Aid_Record__c>{f1});

        //see if it matched
        f1 = requeryFinancial(f1);
        system.assertEquals(c1.Id, f1.Contact__c);
    }




       public static Financial_Aid_Record__c requeryFinancial(Financial_Aid_Record__c fIn) {
        return [
            SELECT Id, Contact__c, studentId__c
            FROM Financial_Aid_Record__c WHERE Id=:fIn.Id
        ];
    }
    
    
    
    	static testmethod void testFinancialContactBatch(){
        list<Financial_Aid_Record__c> b =new list<Financial_Aid_Record__c>();
        for(integer i=0;i<200;i++){
            Financial_Aid_Record__c s=new Financial_Aid_Record__c();
            s.Mainframe_Dataload_ID__c = '123584'+ i;
            s.studentId__c='898523569';
            b.add(s);
        }
        insert b;
           
  		Test.startTest();
      	FinancialContactBatch y = new FinancialContactBatch();
    	database.executeBatch(y);
    	Test.stopTest();
    }
}