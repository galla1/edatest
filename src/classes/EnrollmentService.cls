public without sharing class EnrollmentService {


    //Enrollment - Search for a Contact where Enrollment.studentid = contact.X89_Number_LSU_ID__c and populate the enrollment.contact__c lookup with match or do nothing.
    //studentId__c, X89_Number_LSU_ID__c

    public static void matchEnrollments(List<Enrollment__c> enrollments) {

        List<Enrollment__c> enrollmentsToUpdate = new List<Enrollment__c>();

        //set of LSUID__c's
        Set<String> lsuIds = new Set<String>();

        for (Enrollment__c e: enrollments) {
            if (String.isNotBlank(e.studentId__c)) {
                lsuIds.add(e.studentId__c);
            }
        }

        //Some contact maps to easily look up the values we want
        Map<String, List<Contact>> lsuIdMap = new Map<String, List<Contact>>();

        //query the contacts table, build a map
        for (Contact c: [SELECT Id, X89_Number_LSU_ID__c, CreatedDate
                         FROM Contact
                         WHERE X89_Number_LSU_ID__c IN:lsuIds]) {

            if (String.isNotBlank(c.X89_Number_LSU_ID__c)) {
                if (lsuIdMap.containsKey(c.X89_Number_LSU_ID__c)) {
                    List<Contact> tempList = lsuIdMap.get(c.X89_Number_LSU_ID__c);
                    tempList.add(c);
                    lsuIdMap.put(c.X89_Number_LSU_ID__c, tempList);
                } else {
                    lsuIdMap.put(c.X89_Number_LSU_ID__c, new List<Contact>{c});
                }
            }
        }

        //sort the lists in the map by CreatedDate ASC
        for (String lsuId: lsuIdMap.keySet()) {
            List<Contact> tempList = lsuIdMap.get(lsuId);
            tempList = sortContacts(tempList);
            lsuIdMap.put(lsuId, tempList);
        }


        //go through enrollments to match on contacts
        for (Enrollment__c e: enrollments) {
            //look for existing contact matching Enrollment__c.studentId__c on Contact.X89_Number_LSU_ID__c
            //if multiples found, use oldest CreatedDate

            if (String.isNotBlank(e.studentId__c)) {
                if (lsuIdMap.containsKey(e.studentId__c)) {
                    List<Contact> tempList = lsuIdMap.get(e.studentId__c);
                    e.Contact__c = tempList[0].Id; //get the first in the list, the oldest
                    enrollmentsToUpdate.add(e);
                }
            }
        }

        Boolean doUpdate = false;
        if (Test.isRunningTest()) {
            doUpdate = true;
        } else {
            if (System.isBatch()) {
                doUpdate = true;
            } /*else {
                if (Trigger.isAfter) {
                    doUpdate = true;
               }
            }*/
        }

        if (doUpdate) {
            update enrollmentsToUpdate;
        }

    }


    public static List<Contact> sortContacts(List<Contact> contactsIn) {
        List<ContactWrap> wrapList = new List<ContactWrap>();
        List<Contact> contactsOut = new List<Contact>();

        for (Contact c: contactsIn) {
            wrapList.add(new ContactWrap(c));
        }
        wrapList.sort();

        for (ContactWrap w: wrapList) {
            contactsOut.add(w.c);
        }
        return contactsOut;
    }

    public class ContactWrap implements Comparable {
        public Contact c {get; set;}
        public ContactWrap(Contact con) {
            c = con;
        }
        //sorts by ASC
        public Integer compareTo(Object compareTo) {
            ContactWrap compareToContact = (ContactWrap)compareTo;
            if (c.CreatedDate > compareToContact.c.CreatedDate) {
                return 1;
            }
            if (c.CreatedDate < compareToContact.c.CreatedDate) {
                return -1;
            }
            return 0;
        }
    }




}